<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ("halaman.register");
    }
    public function welcome(Request $request){
        $Name = $request ["nama1"];
        $biodata = $request ["nama2"];

        return view ('halaman.welcome', compact('Name','biodata'));
    }
}
