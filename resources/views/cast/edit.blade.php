@extends('layout.master')
@section('Judul')
    Halaman Edit Cast berid {{$cast->id}}
@endsection
@section('Isi')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">Nama Cast</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}"id="title" placeholder="Masukkan Title">
        @error("nama")
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">umur</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="body" placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">bio</label>
        <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="body" placeholder="Masukkan bio">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection