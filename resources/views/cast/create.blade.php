@extends('layout.master')
@section('Judul')
    Halaman List Cast
@endsection
@section('Isi')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Nama Cast</label>
        <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
        @error("nama")
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">umur</label>
        <input type="text" class="form-control" name="umur" id="body" placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">bio</label>
        <input type="text" class="form-control" name="bio" id="body" placeholder="Masukkan bio">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection